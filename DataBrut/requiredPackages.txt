-- The following REQUIRED packages have been found:

 * PythonInterp (required version >= 3.8)
 * ECM (required version >= 5.22)
 * KF5Config (required version >= 5.44.0)
 * KF5WidgetsAddons (required version >= 5.44.0)
 * KF5Completion (required version >= 5.44.0)
 * KF5CoreAddons (required version >= 5.44.0)
 * KF5GuiAddons (required version >= 5.44.0)
 * Gettext
 * KF5I18n (required version >= 5.44.0)
 * KF5ItemModels (required version >= 5.44.0)
 * KF5ItemViews (required version >= 5.44.0)
 * KF5WindowSystem (required version >= 5.44.0)
 * KF5 (required version >= 5.44.0)
 * Qt5Core
 * Qt5Gui
 * Qt5Widgets
 * Qt5Xml
 * Qt5Network
 * Qt5PrintSupport
 * Qt5Svg
 * Qt5Test
 * Qt5Concurrent
 * Qt5Sql
 * Qt5 (required version >= 5.9.0)
 * PNG
 * Boost (required version >= 1.55)
 * Eigen3 (required version >= 3.0), C++ template library for linear algebra, <http://eigen.tuxfamily.org>
 * LibExiv2 (required version >= 0.16), Image metadata support, <http://www.exiv2.org>
 * LCMS2 (required version >= 2.4), LittleCMS Color management engine, <http://www.littlecms.com>
   Will be used for color management and is necessary for Krita
 * QuaZip (required version >= 0.6), A library for reading and writing zip files, <https://stachenov.github.io/quazip/>
   Needed for reading and writing KRA and ORA files
