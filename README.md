# KritaMGL7460-H20

### Projet Individuel sur Krita de Marc-André Boudreault dans le cadre du cours [MGL7460-H20](https://github.com/ace-lectures/H20-MGL-7460).

### [Rapport final](MGL7460ProjetIndividuel.pdf)
    Ce document contient le rapport final du projet individuel. 

## [Reproductibilité des résultats](References/ReproductibiliteDesResultats.pdf)
    Ce document permet de connaître la façon de récupérer les données, fichiers du rapport ainsi que 
    les procédures d'exécution des scripts et outils utilisés.
    
### [Rapport intermédiaire](V1/MGL7460ProjetIndividuel.pdf)
    Ce document contient la version préliminaire du projet.
### [Corrigé du rapport intermédiaire](V1/MGL7460CorrigeIntermediare.pdf)
    Ce document contient les commentaires de la version préliminaire.
     
## Arborescence
####[DataBrut](DataBrut)
    Contient les données brutes extraites des différents dépôts soit par des outils
    soit de façon manuelle.

####[DataCleaned](DataCleaned)
    Contient les données traitées sur les données brutes. Dans le cas des fichiers Excel 
    l'état peut être différents des graphiques du rapport puisque le forage est dynamique.

####[Images](Images)
    Contient des images ou des formats d'image qui ont servi à étoffer ou embellir le rapport.

####[References](References)
    Contient des documents autres que des sites web.

####[V1](V1)
    Contient la version préliminaire du rapport ainsi que les commentaires obtenus 
    de Sébastien Mosser
    
### Site Web du projet Krita
  http://www.krita.org    
![Picture](https://krita.org/wp-content/uploads/2016/04/krita-30-screenshot.jpg)    